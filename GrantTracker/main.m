//
//  main.m
//  GrantTracker
//
//  Created by HSIUNG, KEVIN on 2/15/14.
//  Copyright (c) 2014 HSIUNG, KEVIN. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
